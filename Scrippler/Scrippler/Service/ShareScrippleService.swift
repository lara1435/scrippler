//
//  ScippleShareService.swift
//  SignInputApp
//
//  Created by Lakshmi Narayanan N on 16/10/22.
//

import SwiftUI
import MultipeerConnectivity

final class  ShareScrippleService: NSObject, ObservableObject {
    private let serviceType = "mpos-service"
    
    fileprivate let currentPeerID: MCPeerID
    let session: MCSession
    fileprivate let browser: MCNearbyServiceBrowser
    fileprivate let advertiser: MCNearbyServiceAdvertiser
    
    @Published var paired: Bool = false
    @Published var availablePeers: [MCPeerID] = []
    @Published var pairedPeers: [MCPeerID] = []
    @Published var receivedInvite: Bool = false
    @Published var receivedInviteFrom: MCPeerID? = nil
    @Published var selectedPeer: MCPeerID? = nil
    @Published var invitationHandler: ((Bool, MCSession?) -> Void)!
    
    var receivedScripple: (([Line]) -> Void)?
    
    init(name: String) {
        currentPeerID = MCPeerID(displayName: name)
        
        session = MCSession(peer: currentPeerID,
                            securityIdentity: nil,
                            encryptionPreference: .none)
        advertiser = MCNearbyServiceAdvertiser(peer: currentPeerID,
                                               discoveryInfo: nil,
                                               serviceType: serviceType)
        browser = MCNearbyServiceBrowser(peer: currentPeerID,
                                         serviceType: serviceType)
        super.init()
        
        session.delegate = self
        advertiser.delegate = self
        browser.delegate = self
    }
    
    deinit {
        session.delegate = nil
        advertiser.delegate = nil
        browser.delegate = nil
    }
}

extension ShareScrippleService {
    func startBrowsing() {
        browser.startBrowsingForPeers()
    }
    
    func stopBrowsing() {
        availablePeers = []
        browser.stopBrowsingForPeers()
    }

    func startAdvertising() {
        advertiser.startAdvertisingPeer()
    }
    
    func stopAdvertising() {
        advertiser.stopAdvertisingPeer()
    }

    func invitePeer(_ peer: MCPeerID) {
        browser.invitePeer(peer, to: session, withContext: nil, timeout: 30)
    }
    
    func acceptInvite() {
        invitationHandler(true, session)
    }
    
    func rejectInvite() {
        invitationHandler(false, nil)
    }
    
    func sendToAll(_ scripple: [Line]) {
        do {
            let scrippleData = try JSONEncoder().encode(scripple)
            try session.send(scrippleData,
                             toPeers: session.connectedPeers,
                             with: .reliable)
        } catch {
            print("Error sending: \(String(describing: error))")
        }
    }
    
    func sendScripple(_ scripple: [Line], to peer: MCPeerID) {
        do {
            let scrippleData = try JSONEncoder().encode(scripple)
            try session.send(scrippleData,
                             toPeers: [peer],
                             with: .reliable)
        } catch {
            print("Error sending: \(String(describing: error))")
        }
    }
}

extension ShareScrippleService: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print("peer \(peerID) didChangeState: \(state.rawValue)")
        
        switch state {
        case MCSessionState.notConnected:
            // Peer disconnected
            DispatchQueue.main.async {
                self.paired = false
                self.pairedPeers = self.pairedPeers.filter({ $0 !== peerID })
                
            }
            
            break
        case MCSessionState.connected:
            // Peer connected
            DispatchQueue.main.async {
                self.paired = true
                self.pairedPeers.append(peerID)
            }
            
            break
        default:
            // Peer connecting or something else
            DispatchQueue.main.async {
                self.paired = false
                self.pairedPeers = self.pairedPeers.filter({ $0 !== peerID })
            }
            break
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        do {
            let scrippleData = try JSONDecoder().decode([Line].self, from: data)
            receivedScripple?(scrippleData)
        } catch {
            print("Currepted Data")
        }
    }
    
    public func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        print("Receiving streams is not supported")
    }
    
    public func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        print("Receiving resources is not supported")
    }
    
    public func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        print("Receiving resources is not supported")
    }
    
    public func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
        certificateHandler(true)
    }
}

extension ShareScrippleService: MCNearbyServiceBrowserDelegate {
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print("Browser not Started Browsing: \(String(describing: error))")
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        DispatchQueue.main.async {
            self.availablePeers.append(peerID)
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print("Browser lost peer: \(peerID)")
        
        DispatchQueue.main.async {
            self.availablePeers.removeAll(where: {
                $0 == peerID
            })
        }
    }
}

extension ShareScrippleService: MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        print("Advertiser not Started advertising: \(String(describing: error))")
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        print("Advertiser received Invitation from peer: \(peerID)")
        print("current peerID: \(currentPeerID)")
        
        DispatchQueue.main.async {
            self.receivedInvite = true
            self.receivedInviteFrom = peerID
            self.invitationHandler = invitationHandler
        }
    }
}
