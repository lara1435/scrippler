//
//  ScripplerApp.swift
//  Scrippler
//
//  Created by Lakshmi Narayanan N on 17/10/22.
//

import SwiftUI

@main
struct ScripplerApp: App {
    @ObservedObject var shareService = ShareScrippleService(name: UIDevice.current.name)
    
    var body: some Scene {
        WindowGroup {
            HomeView()
                .environmentObject(shareService)
        }
    }
}
