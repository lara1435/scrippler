//
//  Line.swift
//  Scrippler
//
//  Created by Lakshmi Narayanan N on 17/10/22.
//

import SwiftUI

struct Line: Identifiable {
    let id = UUID()
    
    var points: [CGPoint]
    var color: Color
    var lineWidth: CGFloat
}

extension Line: Codable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        points = try values.decode([CGPoint].self, forKey: .points)
        color = try values.decode(Color.self, forKey: .color)
        lineWidth = try values.decode(CGFloat.self, forKey: .lineWidth)
    }
    
    enum CodingKeys: String, CodingKey {
        case points
        case color
        case lineWidth
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(points, forKey: .points)
        try container.encode(color, forKey: .color)
        try container.encode(lineWidth, forKey: .lineWidth)
    }
}
