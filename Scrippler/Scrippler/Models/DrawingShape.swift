//
//  DrawingShape.swift
//  Scrippler
//
//  Created by Lakshmi Narayanan N on 17/10/22.
//

import SwiftUI

struct DrawingShape: Shape {
    let points: [CGPoint]
    
    func path(in rect: CGRect) -> Path {
        createPath(for: points)
    }
    
    func createPath(for points: [CGPoint]) -> Path {
        var path = Path()
        
        if let firstPoint = points.first {
            path.move(to: firstPoint)
        }
        
        for index in 1..<points.count {
            let mid = calculateMidPoint(points[index - 1], points[index])
            path.addQuadCurve(to: mid, control: points[index - 1])
        }
        
        if let last = points.last {
            path.addLine(to: last)
        }
        
        return path
    }
    
    func calculateMidPoint(_ point1: CGPoint, _ point2: CGPoint) -> CGPoint {
        let newMidPoint = CGPoint(x: (point1.x + point2.x)/2, y: (point1.y + point2.y)/2)
        return newMidPoint
    }
}
