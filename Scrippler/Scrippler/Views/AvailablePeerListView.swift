//
//  PeerListView.swift
//  SignInputApp
//
//  Created by Lakshmi Narayanan N on 16/10/22.
//

import SwiftUI

struct AvailablePeerListView: View {
    
    @EnvironmentObject var shareService: ShareScrippleService
    
    var body: some View {
        NavigationStack {
            HStack {
                List(shareService.availablePeers, id: \.self) { peer in
                    Button {
                        if shareService.session.connectedPeers.contains([peer]) {
                            return
                        } else {
                            shareService.invitePeer(peer)
                        }
                    } label: {
                        HStack {
                            Text(peer.displayName)
                            Spacer()
                            Text(shareService.session.connectedPeers.contains([peer]) ? "Connected" : "")
                            
                        }
                    }
                }
            }
            .navigationTitle("Available Peers")
        }
        .onAppear {
            shareService.stopAdvertising()
            shareService.startBrowsing()
        }
    }
}

struct PeerListView_Previews: PreviewProvider {
    static var previews: some View {
        AvailablePeerListView()
            .environmentObject(ShareScrippleService(name: UIDevice.current.name))
    }
}
