//
//  DrawingView.swift
//  SignInputApp
//
//  Created by Lakshmi Narayanan N on 10/10/22.
//

import SwiftUI
import UIKit

struct DrawingView: View {
    
    @Binding public var lines :[Line]
    @Binding public var selectedColor: Color
    @Binding public var lineWidth: Double
    
    var body: some View {
        NavigationStack {
            ZStack {
                Color.white
                ForEach(lines){ line in
                    DrawingShape(points: line.points)
                        .stroke(line.color, style: StrokeStyle(lineWidth: line.lineWidth, lineCap: .round, lineJoin: .round))
                }
            }
            .gesture(DragGesture(minimumDistance: 0, coordinateSpace: .local).onChanged({ value in
                let newPoint = value.location
                if value.translation.width + value.translation.height == 0 {
                    lines.append(Line(points: [newPoint], color: selectedColor, lineWidth: lineWidth))
                } else {
                    let index = lines.count - 1
                    lines[index].points.append(newPoint)
                }
                
            }).onEnded({ value in
                if let last = lines.last?.points, last.isEmpty {
                    lines.removeLast()
                }
            })
            )
        }
    }
}

struct DrawingView_Previews: PreviewProvider {
    static var previews: some View {
        DrawingView(lines:  .constant([]), selectedColor: .constant(.green), lineWidth: .constant(2))
    }
}
