//
//  HomeView.swift
//  SignInputApp
//
//  Created by Lakshmi Narayanan N on 10/10/22.
//

import SwiftUI

struct HomeView: View {
    
    @EnvironmentObject var shareService: ShareScrippleService
    
    @State var selectedColor = Color.green
    @State var lineWidth = 2.0
    @State private var lines = [Line]()
    
    var body: some View {
        NavigationStack {
            ZStack {
                Color.white
                drawingView
            }
            .navigationTitle("Scrippler")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    ColorPicker("", selection: $selectedColor)
                }
                
                ToolbarItem(placement: .navigationBarLeading) {
                    Slider(value: $lineWidth, in: 0...10)
                        .frame(width: 100)
                }
                
                ToolbarItem(placement: .navigationBarTrailing) {
                    Menu(content: {
                        
                        if !shareService.pairedPeers.isEmpty {
                            Menu("Connected Peers") {
                                ForEach(shareService.pairedPeers, id: \.self) { peer in
                                    Button(peer.displayName) {
                                        shareService.sendScripple(lines, to: peer)
                                    }
                                }
                                
                                if !shareService.pairedPeers.isEmpty {
                                    Button("All") {
                                        shareService.sendToAll(lines)
                                    }
                                    
                                }
                            }
                        }
                       

                        NavigationLink("Browse") {
                            AvailablePeerListView()
                        }
                        
                        Button("Connect") {
                            shareService.startAdvertising()
                        }
                    }, label: {
                        Text("Share")
                        
                    })
                }
            }
            .toolbarBackground(.visible, for: .navigationBar)
            .toolbarBackground(.visible, for: .bottomBar)
            .ignoresSafeArea()
            .alert("Received an invite from \(shareService.receivedInviteFrom?.displayName ?? "ERR")!",
                   isPresented: $shareService.receivedInvite) {
                Button("Accept invite") {
                    if (shareService.invitationHandler != nil) {
                        shareService.acceptInvite()
                    }
                }
                Button("Reject invite") {
                    if (shareService.invitationHandler != nil) {
                        shareService.rejectInvite()
                    }
                }
            }
        }
        .onAppear{
            shareService.receivedScripple = { lines in
                self.lines = lines
            }
        }
    }
    
    var drawingView: some View {
        DrawingView(lines: $lines, selectedColor: $selectedColor, lineWidth: $lineWidth)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
